var colors = ["#3D9970", "#01FF70", "#7FDBFF", "#001f3f"]

function renderPage(name) {
  var page = story[name]
  if (page === undefined) {
    alert("Reached impossible " + name)
    return
  }
  var messageHolder = document.getElementById("messageHolder")
  messageHolder.innerHTML = page.message
  var variantsHolder = document.getElementById("variantsHolder")
  var table = document.createElement("table")

  for (var i = 0; i < page.variants.length; i++) {
    var variant = page.variants[i]
    var row = document.createElement("tr")
    row.style = "color:" + colors[i % colors.length]
    var cell = document.createElement("td")
    cell.style = "; border:1px solid black"
    cell.innerHTML = variant.message
    cell.id = variant.next
    cell.addEventListener("click", function (e) { renderPage(variant.next)})
    row.appendChild(cell)
    table.appendChild(row)
  }
  var oldVariants = variantsHolder.childNodes[0]
  variantsHolder.replaceChild(table, oldVariants)
}
