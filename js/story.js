var story = {
  "first":  {
    message: `Ты на первой странице. <br>
Where would you go?`,
    variants: [
      {next: "left", message: "I go left"},
      {next: "right", message: "I go right"}
    ]
  },
  "left":  {
    message: `You came to the left room
<img src="https://medialeaks.ru/wp-content/uploads/2017/10/catbread-03-600x400.jpg">`,
    variants: [
      {next: "first", message: "I go back"},
      {next: "right", message: "I got to unknown"}
    ]
  },
  "right": {
    message: `You came to the right room`,
    variants: [
      {next: "right2", message: 'Next <img src="https://medialeaks.ru/wp-content/uploads/2017/10/catbread-08-470x500.jpg">'}
    ]
  },
  "right2": {
    message: `You came to next right`,
    variants: [
      {next: "deadend", message: "Go to oblivion"},
      {next: "left", message: "I go left"}
    ]
  },
  "deadend": {
    message: `You came to deadend`,
    variants: [
    ]
  }
}
